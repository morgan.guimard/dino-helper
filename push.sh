#!/usr/bin/env sh

VERSION=$1
git tag $VERSION
git push origin $VERSION
docker build -t mguimard/dino-helper:latest .
docker tag mguimard/dino-helper:latest mguimard/dino-helper:$VERSION
docker push mguimard/dino-helper:$VERSION
docker push mguimard/dino-helper:latest
 