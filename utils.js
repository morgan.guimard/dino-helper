const request = require('request');
const fs = require('fs');
const quotes = '```';

module.exports = {
    getJson: (url) => {
        return new Promise((resolve, reject) => {
            request.get({
                url: url,
                json: true,
                headers: { 'User-Agent': 'request' }
            }, (err, res, data) => {
                if (err) {
                    reject(err);
                } else if (res.statusCode !== 200) {
                    reject(res);
                } else {
                    resolve(data);
                }
            });
        });
    },
    findDinos: (data, name) => Object.keys(data.Species).filter(
        cName => cName.toLowerCase().match(name.toLowerCase()) ||  data.Species[cName].Name && data.Species[cName].Name.toLowerCase().match(name.toLowerCase())
    ).map(cName => data.Species[cName]),
    quote: (s) => `${quotes}${s}${quotes}`
};
