const Discord = require('discord.io');
const logger = require('winston');
const table = require('text-table');
const { quote, getJson, findDinos } = require('./utils');
const { base_url, maps, token, loggerLevel } = require('./config.json');

logger.remove(logger.transports.Console);

logger.add(new logger.transports.Console, {
    colorize: true
});

logger.level = loggerLevel;

logger.info('Connecting');

let bot = new Discord.Client({
    token: token,
    autorun: true
});

bot.on('disconnect', () => {
    logger.info('Disconnected');
});

bot.on('ready', (evt) => {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});

bot.on('message', (user, userID, channelID, msg, evt) => {
    if (msg.substring(0, 1) === '!') {
        let args = msg.substring(1).split(' ');
        let cmd = args[0];

        switch (cmd) {

            case 'dino':

                let map = args[1];
                let dino = args[2];

                if (map === 'help' || !dino) {

                    let message = 'Usage :\n';
                    message += '!dino <map> <dino>\n';
                    message += `Available maps:\n${Object.keys(maps).map(k => '- ' + k + ' : ' + maps[k]).join('\n')}`;

                    bot.sendMessage({
                        to: channelID,
                        message: quote(message)
                    });

                    break;
                }

                let url = `${base_url}${maps[map]}?t=${Date.now()}`;

                logger.info(`Request: ${url}`);

                getJson(url).then(data => {
                    const dinos = findDinos(data, dino);

                    logger.info(`Results: ${dinos.length} species`);

                    if (!dinos.length) {
                        message = `Sorry, no results for ${dino} on ${map}`;
                        bot.sendMessage({
                            to: channelID,
                            message: quote(message)
                        });
                    }

                    dinos.forEach(dinoList => {
                        let rows = [['Lvl', 'Gender', 'Lat', 'Lon']];

                        dinoList.Creatures.slice(0, 10).forEach(dino => {
                            rows.push([dino.BaseLevel, dino.Gender, dino.Latitude.toFixed(1), dino.Longitude.toFixed(1)]);
                        });

                        let message = `${dinoList.Creatures.length} ${dinoList.Name || dinoList.ClassName}: \n${table(rows)}`;

                        bot.sendMessage({
                            to: channelID,
                            message: quote(message)
                        });
                    });
                });
                break;

            case 'remind':

                let usage = () => {
                    let message = 'Usage :\n!remind <minutes> <message>';
                    bot.sendMessage({
                        to: channelID,
                        message: quote(message)
                    });
                }

                if (args[1] === 'help' || !args[2]) {
                    usage();
                    break;
                }

                let timeout;

                try {
                    let minutes = parseFloat(args[1]);
                    if(minutes > 24 * 60){
                        bot.sendMessage({
                            to: channelID,
                            message: quote('Cannot register timers longer than 24h')
                        })
                        break;
                    }
                    timeout =  minutes * 60 * 1000;
                } catch (err) {
                    usage();
                    break;
                }

                bot.sendMessage({
                    to: channelID,
                    message: quote('Timer registered')
                });

                let sentence = args.splice(2, args.length - 1).join(' ');

                setTimeout(() => {
                    bot.sendMessage({
                        to: channelID,
                        message: quote(sentence)
                    });
                }, timeout);

                break;
        }
    }
});
