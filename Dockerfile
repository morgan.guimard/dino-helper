FROM node:8-alpine AS build_env
WORKDIR /usr/src/app
COPY package.json ./
RUN yarn install --production
COPY utils.js bot.js ./
CMD ["node", "bot.js"]
